from django.conf.urls import patterns, include, url
from django.conf import settings
from registration.views import register

from django.contrib import admin
admin.autodiscover()

from .views import HomeView
from profiles.forms import CustomEmailRegistrationForm
from profiles.views import UserProfileUpdateView, UserUpdateView, MyResume

urlpatterns = patterns('',
    # url(r'^$', 'gtkm.views.home', name='home'),
    # url(r'^gtkm/', include('gtkm.foo.urls')),
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^resume/$', MyResume.as_view(), name='resume_simple'),
    url(r'^dl/resume.pdf$', 'profiles.views.generate_pdf', name='resume_download'),
    url(r'^users/', include('profiles.urls')),
    url(r'^accounts/profile/$', UserProfileUpdateView.as_view(), name='accounts_profile'),
    url(r'^accounts/main/$', UserUpdateView.as_view(), name='accounts_main'),

    url(r'^accounts/register/$',
        register,
        {'backend': 'registration.backends.simple.SimpleBackend',
        'template_name': 'registration/registration_form.html',
        'form_class': CustomEmailRegistrationForm,
        'success_url': getattr(
        settings, 'REGISTRATION_EMAIL_REGISTER_SUCCESS_URL', None),
        },
        name='registration_registe',
        ),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('registration_email.backends.default.urls')),
)
