from django.views.generic import TemplateView, ListView
#from braces.views import SetHeadlineMixin
#from django.template import RequestContext
#from django.shortcuts import get_object_or_404, render_to_response
#from django.contrib.auth.models import User

class HomeView(TemplateView):
    template_name = "home.html"

class ResumeSimple(TemplateView):
    template_name = "resume-simple.html"

#class AboutView(SetHeadlineMixin, TemplateView):
#    template_name = 'about.html'
#    headline = 'About Us'
#
#
#class ContactView(SetHeadlineMixin, TemplateView):
#    template_name = 'contact.html'
#    headline = 'Contact Us'
