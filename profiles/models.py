from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

class UserProfile(models.Model):
    SEX_CHOICES = (
            ('M', 'Male'),
            ('F', 'Female'),
    )

    user = models.OneToOneField(User)
    sex = models.CharField(max_length=1, choices=SEX_CHOICES, default='M')
    dob = models.DateField('Date of Birth', null=True, blank=True)
    designation = models.CharField(max_length=30, null=True, blank=True)
    bio = models.TextField('About You', blank=True, default='')
    gravatar_email = models.EmailField(null=True, blank=True)
    contact_email = models.EmailField(null=True, blank=True)
    phone = models.BigIntegerField(null=True, blank=True)
    available = models.BooleanField(default=False)

    def __unicode__(self):
        return self.user.username

    def get_absolute_url(self):
        return reverse('user_detail', kwargs={'username': self.user.username})

    def get_address(self):
        try:
            return self.user.address
        except ObjectDoesNotExist:
            return None

    def get_education(self):
        return self.user.educations.all()

    def get_social_profiles(self):
        return self.user.social_profiles.all()

    def get_projects(self):
        return self.user.projects.all()


    def get_experiences(self):
        return self.user.experiences.all()


class Address(models.Model):
    user = models.OneToOneField(User)
    street = models.CharField(max_length=255, null=True, blank=True)
    city = models.CharField(max_length=60, null=True, blank=True)
    state = models.CharField(max_length=60, null=True, blank=True)
    country = models.CharField(max_length=60, null=True, blank=True, default="India")

    def __unicode__(self):
        return unicode("%s - %s, %s, %s" %(self.user, self.street, self.city, self.country))


class Project(models.Model):
    user = models.ForeignKey(User, related_name="projects")
    name = models.CharField(max_length=255, null=True, blank=True)
    url = models.URLField(null=True, blank=True)
    about = models.TextField('About You', blank=True, default='')

    def __unicode__(self):
        return unicode("%s - %s" %(self.user, self.name))


class Education(models.Model):
    user = models.ForeignKey(User, related_name="educations")
    course = models.CharField(max_length=255, null=True, blank=True)
    school = models.CharField(max_length=255, null=True, blank=True)
    start_year = models.PositiveIntegerField(null=True, blank=True)
    end_year = models.PositiveIntegerField(null=True, blank=True)
    about = models.TextField('About You', blank=True, default='')

    def __unicode__(self):
        return unicode("%s - %s" %(self.user, self.course))


class Experience(models.Model):
    user = models.ForeignKey(User, related_name="experiences")
    position = models.CharField(max_length=255, null=True, blank=True)
    company = models.CharField(max_length=255, null=True, blank=True)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    about = models.TextField('About You', blank=True, default='')

    def __unicode__(self):
        return unicode("%s - %s in %s" %(self.user, self.position, self.company))


#class Skills(models.Model):
#    name = models.CharField(max_length=255)
#
#   def __unicode__(self):
#       return unicode(self.name)


class SocialProfile(models.Model):
    user = models.ForeignKey(User, related_name="social_profiles")
    label = models.CharField(max_length=45, null=True, blank=True)
    url = models.URLField()

    def __unicode__(self):
        return unicode("%s %s" %(self.user, self.url))


#SIGNALS

from django.dispatch import receiver
from guardian.shortcuts import assign_perm
from registration.signals import user_registered

@receiver(user_registered)
def create_user_profile(sender, user, request, **kwargs):
    user.first_name = request.POST.get('first_name')
    user.last_name = request.POST.get('last_name')
    user.save()
    user_profile = UserProfile.objects.create(user=user)
    assign_perm('change_userprofile', user, user_profile)
    assign_perm('delete_userprofile', user, user_profile)
    assign_perm('change_user', user, user)
