from django import forms
from registration_email.forms import EmailRegistrationForm
from django.contrib.auth.forms import ReadOnlyPasswordHashField

from django.contrib.auth.models import User
from .models import UserProfile

class CustomEmailRegistrationForm(EmailRegistrationForm):
    first_name = forms.CharField()
    last_name = forms.CharField()

class UserUpdateForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')

class UserProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        exclude = ['user',]
