from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse, reverse_lazy
from django.contrib import messages
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.views.generic import DetailView, RedirectView, TemplateView, ListView, UpdateView, CreateView, DeleteView
from guardian.mixins import LoginRequiredMixin, PermissionRequiredMixin
#from braces.views import SetHeadlineMixin
import os
from django.conf import settings
from django.template import Context
from django.template.loader import get_template

from xhtml2pdf import pisa
#
from django.contrib.auth.models import User
from .forms import UserProfileUpdateForm, UserUpdateForm
from .models import UserProfile

class UserDetail(LoginRequiredMixin, DetailView):
    context_object_name = 'profile'
    template_name = 'profiles/user_detail.html'
    model = User

    def get_object(self):
        user = get_object_or_404(User, username=self.kwargs['username'])
        if user.is_active:
            return user
        else:
            raise Http404

    def get_context_data(self, **kwargs):
        context = super(UserDetail, self).get_context_data(**kwargs)
        user = get_object_or_404(User, username=self.kwargs['username'])
        userprofile = get_object_or_404(UserProfile, user=user) #redundancy check for user may exist but his profile not
        context['userprofile'] = userprofile
        return context


class UserUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    form_class = UserUpdateForm
    model = User
    template_name = 'profiles/user_form.html'
    permission_required = 'auth.change_user'
    return_403 = True

    def get_object(self):
        return self.request.user

    def get_success_url(self):
        messages.success(self.request, 'Your account have been saved')
        return reverse_lazy('user_update')


class UserProfileUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    form_class = UserProfileUpdateForm
    model = UserProfile
    template_name = 'profiles/user_form.html'
    permission_required = 'profiles.change_userprofile'
    return_403 = True

    def get_object(self):
        return self.request.user.userprofile
#        user = get_object_or_404(User, username=self.kwargs['username'])
#        userprofile = get_object_or_404(UserProfile, user=user)
#        if user.is_active:
#            return userprofile
#        else:
#            raise Http404

    def get_success_url(self):
        messages.success(self.request, 'Your profile have been saved.')
        return reverse_lazy('accounts_profile')


class MyResume(LoginRequiredMixin, TemplateView):
    template_name = 'profiles/user_detail.html'

    def get_context_data(self, **kwargs):
        context = super(MyResume, self).get_context_data(**kwargs)
        user = self.request.user
        userprofile = get_object_or_404(UserProfile, user=user) #redundancy check for user may exist but his profile not
        context['profile'] = user
        context['userprofile'] = userprofile
        return context


# Convert HTML URIs to absolute system paths so xhtml2pdf can access those resources
def link_callback(uri, rel):
    # use short variable names
    sUrl = settings.STATIC_URL      # Typically /static/
    sRoot = settings.STATIC_ROOT    # Typically /home/userX/project_static/
    mUrl = settings.MEDIA_URL       # Typically /static/media/
    mRoot = settings.MEDIA_ROOT     # Typically /home/userX/project_static/media/

    # convert URIs to absolute system paths
    #if uri.startswith(mUrl):
    #    path = os.path.join(mRoot, uri.replace(mUrl, ""))
    if uri.startswith(sUrl):
        path = os.path.join(sRoot, uri.replace(sUrl, ""))

    # make sure that file exists
    if not os.path.isfile(path):
            raise Exception(
                    'media URI must start with %s or %s' % \
                    (sUrl, mUrl))
    return path

def generate_pdf(request):
    # Prepare context
    context = {}
    user = request.user
    userprofile = get_object_or_404(UserProfile, user=user) #redundancy check for user may exist but his profile not
    context['profile'] = user
    context['userprofile'] = userprofile

    # Render html content through html template with context
    template = get_template('pdf/resume.html')
    html  = template.render(Context(context))

    # Write PDF to file
    file = open(os.path.join(settings.MEDIA_ROOT, 'test.pdf'), "w+b")
    pisaStatus = pisa.CreatePDF(html, dest=file,
            link_callback = link_callback)

    # Return PDF document through a Django HTTP response
    file.seek(0)
    pdf = file.read()
    file.close()            # Don't forget to close the file handle
    return HttpResponse(pdf, mimetype='application/pdf')
