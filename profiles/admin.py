from django.contrib import admin

from .models import UserProfile, Address, Project, Education, Experience, SocialProfile

class UserProfileAdmin(admin.ModelAdmin):

    list_display = ('user', 'sex', 'designation')
    list_filter = ('sex',)
    search_fields = ('first_name', 'lat_name', 'designation', 'bio')
    ordering = ('user',)

admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Address)
admin.site.register(Project)
admin.site.register(Education)
admin.site.register(Experience)
admin.site.register(SocialProfile)
